package org.calyxos.backup.contacts;

import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackupDataOutput;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ContactsBackupAgent extends BackupAgent {

    private final static String TAG = "ContactsBackupAgent";
    private final static String BACKUP_FILE = "backup-v1.tar";
    final static boolean DEBUG = true; // don't commit with true

    @Override
    public void onFullBackup(FullBackupDataOutput data) throws IOException {
        if (shouldAvoidBackup(data)) {
            Log.w(TAG, "onFullBackup will not back up. Flags: " + data.getTransportFlags());
            return;
        } else {
            Log.d(TAG, "onFullBackup will do backup");
        }

        // get VCARDs as an InputStream
        VCardExporter vCardExporter = new VCardExporter(getContentResolver());
        InputStream vCardInputStream = vCardExporter.getVCardInputStream();

        // create backup file as an OutputStream
        File backupFile = new File(getFilesDir(), BACKUP_FILE);
        FileOutputStream backupFileOutputStream = new FileOutputStream(backupFile);

        // write VCARDs into backup file
        try {
            byte[] buf = new byte[8192];
            int length;
            while ((length = vCardInputStream.read(buf)) > 0) {
                backupFileOutputStream.write(buf, 0, length);
                if (DEBUG) {
                    Log.d(TAG, new String(buf, 0, length));
                }
            }
        } finally {
            backupFileOutputStream.close();
            vCardInputStream.close();
        }

        // backup file
        fullBackupFile(backupFile, data);

        // delete file when done
        if (!backupFile.delete()) {
            Log.w(TAG, "Could not delete: " + backupFile.getAbsolutePath());
        }
    }

    private boolean shouldAvoidBackup(FullBackupDataOutput data) {
        boolean isEncrypted = (data.getTransportFlags() & FLAG_CLIENT_SIDE_ENCRYPTION_ENABLED) != 0;
        boolean isDeviceTransfer = (data.getTransportFlags() & FLAG_DEVICE_TO_DEVICE_TRANSFER) != 0;
        return !isEncrypted && !isDeviceTransfer;
    }

    @Override
    public void onRestoreFile(ParcelFileDescriptor data, long size, File destination, int type, long mode, long mtime) throws IOException {
        Log.e(TAG, "onRestoreFile");
        super.onRestoreFile(data, size, destination, type, mode, mtime);

        File backupFile = new File(getFilesDir(), BACKUP_FILE);
        Log.w(TAG, "backupFile exists: " + backupFile.exists());
        Log.w(TAG, "backupFile length: " + backupFile.length());

        try (FileInputStream backupFileInputStream = new FileInputStream(backupFile)) {
            VCardImporter vCardImporter = new VCardImporter(getContentResolver());
            vCardImporter.importFromStream(backupFileInputStream);
        }

        // delete file when done
        if (!backupFile.delete()) {
            Log.w(TAG, "Could not delete: " + backupFile.getAbsolutePath());
        }
    }

    @Override
    public void onRestoreFinished() {
        super.onRestoreFinished();
        Log.e(TAG, "onRestoreFinished");
    }

    @Override
    public void onQuotaExceeded(long backupDataBytes, long quotaBytes) {
        super.onQuotaExceeded(backupDataBytes, quotaBytes);
        Log.e(TAG, "onQuotaExceeded " + backupDataBytes + " / " + quotaBytes);
    }

    /**
     * The methods below are for key/value backup/restore and should never get called
     **/

    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) {
        Log.e(TAG, "onBackup noSuper");
    }

    @Override
    public void onRestore(BackupDataInput data, int appVersionCode, ParcelFileDescriptor newState) {
        Log.e(TAG, "onRestore noSuper");
    }

}
