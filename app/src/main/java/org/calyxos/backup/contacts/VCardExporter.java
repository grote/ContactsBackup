package org.calyxos.backup.contacts;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.provider.ContactsContract.Contacts.CONTENT_MULTI_VCARD_URI;
import static android.provider.ContactsContract.Data.CONTENT_URI;
import static android.provider.ContactsContract.Data.LOOKUP_KEY;
import static android.provider.ContactsContract.RawContacts.ACCOUNT_TYPE;
import static org.calyxos.backup.contacts.ContactsBackupAgent.DEBUG;

class VCardExporter {

    private final static String TAG = "VCardExporter";

    // android.provider.ContactsContract.DataColumns.IS_PRIMARY
    private final static String IS_PRIMARY = "is_primary";

    private final ContentResolver contentResolver;

    VCardExporter(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    InputStream getVCardInputStream() throws FileNotFoundException {
        String lookupKeysStr = String.join(":", getLookupKeys());
        if (DEBUG) {
            Log.d(TAG, "lookupKeysStr: " + lookupKeysStr);
        }
        Uri uri = Uri.withAppendedPath(CONTENT_MULTI_VCARD_URI, Uri.encode(lookupKeysStr));

        return contentResolver.openInputStream(uri);
    }

    private List<String> getLookupKeys() {
        String[] projection = new String[]{LOOKUP_KEY};
        String selection = ACCOUNT_TYPE + " is null AND " + IS_PRIMARY + " = 1";
        Cursor cursor = contentResolver.query(CONTENT_URI, projection, selection, null, null);
        if (cursor == null) {
            Log.e(TAG, "Cursor for LOOKUP_KEY is null");
            return Collections.emptyList();
        }
        ArrayList<String> lookupKeys = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                lookupKeys.add(cursor.getString(0));
            }
        } finally {
            cursor.close();
        }
        return lookupKeys;
    }

}
